Dice'n'Cards
=============

Python package to manage dice rolls and card distributions.

Installation guide
---------------

::

    pip install dicencards

Developer guide
---------------

Dice module
`````````````

::

    import dicencards.dice

Classes
'''''''''''''
The :code:`Die` class represents a single die, while the :code:`BunchOfDice` class represents a pack of dice that can be rolled simultaneously.

Examples
'''''''''''''

**Example 1:** Roll a 20-sided die.

::

    d20 = Die(20)
    result = d20.roll()

**Example 2:** Roll 4 6-sided dice. The dice are *explosive* on the maximum value, i.e. each dice that gets the maximum score is rerolled, and the results accumulate for as long as you get the maximum on that dice.

::

    bunch = BunchOfDice(4, 6)
    result = bunch.roll(UNLIMITED_REROLL, NO_REROLL)

    # Display the result of each die
    print(result[SCORES])

    # Display the best result of all dice rolled
    print('Best of dice -> {}'.format(result[BEST_OF_DICE]))
    # Display the sum of all scores
    print('Sum of dice -> {}'.format(result[SUM_OF_DICE]))


Card module
`````````````

::

    import dicencards.cards

Classes
'''''''''''''
The :code:`Card` class represents a single card.
Class :code:`Deck` represents a deck of cards.
Class :code:`Hand` represents a player's *hand*.
Class :code:`CardValueModel` represents a card value model, so that cards can be valued and classified.

Examples
'''''''''''''

Example: Randomly draw a 5-card hand from a 52-card pack.

::

    # Get a 52-card deck
    deck: Deck = FIFTY_TWO_CARD_DECK_MODEL.build_deck()
    # Shuffle the deck
    deck.shuffle()
    # Draw 5 cards to form a hand
    hand = deck.draw(5)
    # Display the cards in your hand
    print('\t'.join(str(c) for c in hand))
    # Sort the cards in the hand according to a basic valuation model
    value_model = BasicCardValueModel()
    hand = value_model.sort(hand, CardValueModel.DESC)
    print('\t'.join(str(c) for c in hand))

